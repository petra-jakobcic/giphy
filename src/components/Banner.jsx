import React from 'react';
import banner from '../banner.gif';

export default function Banner() {
  return (
    <div className="u-margin-bottom-small">
        <img src={banner} alt="The banner" className="banner" />
    </div>
  )
}
