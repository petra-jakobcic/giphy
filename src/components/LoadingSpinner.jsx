import React from 'react';
import Loader from "react-loader-spinner";
import "react-loader-spinner/dist/loader/css/react-spinner-loader.css";

export default function LoadingSpinner() {
  return (
    <div className="loading-spinner u-margin-bottom-medium">
      <Loader
        type="ThreeDots"
        color="#f61aa0"
        height={40}
        width={80}
      />
    </div>
  );
}
