import React, { useState } from 'react';
import giphy from '../giphy.jpg';
import DropdownMenu from './DropdownMenu';

export default function Header() {
  // When the user hovers over the 'Menu' icon,
  // a dropdown menu is shown.
  const [showMenuForDesktop, setShowMenuForDesktop] = useState(false);
  const [showMenuForMobile, setShowMenuForMobile] = useState(false);

  /**
   * Handles the hover over the dropdown menu on desktop screens.
   *
   * @param {Object} e The event object.
   *
   * @returns {void}
   */
  function handleDropdownMenuDesktopHover(e) {
    (showMenuForDesktop) ? setShowMenuForDesktop(false) : setShowMenuForDesktop(true);
  }

  /**
   * Handles the click on the dropdown menu on mobile screens.
   *
   * @param {Object} e The event object.
   *
   * @returns {void}
   */
  function handleDropdownMenuMobileClick(e) {
    (showMenuForMobile) ? setShowMenuForMobile(false) : setShowMenuForMobile(true);
  }

  return (
    <header className="header">
      <nav className="nav" id="navigation">
        <div className="logo">
          <img src={giphy} className="logo__image" height={50} alt="Giphy logo" />

          <span className="logo__text">Giphy</span>
        </div>

				{/* Quick navigation */}
				<div className="nav__quick-nav--small">
          <div className="nav__icons">
            <div className="nav__icon-wrap">
              <i className="fas fa-plus nav__icon nav__icon--plus"></i>
            </div>

            <div className="nav__icon-wrap">
              <i className="far fa-user nav__icon nav__icon--person"></i>
            </div>

            {/* Burger Menu for Mobile */}
            <div className="nav__icon-wrap" onClick={handleDropdownMenuMobileClick}>
              <i className="fas fa-bars nav__icon nav__icon--menu"></i>
            </div>
          </div>
        </div>

				<div className="nav__quick-nav--big">
          <ul className="nav__links">
            <li className="nav__item">
              <span className="nav__link nav__link--1">Reactions</span>
            </li>

            <li className="nav__item">
              <span className="nav__link nav__link--2">Entertainment</span>
            </li>

            <li className="nav__item">
              <span className="nav__link nav__link--3">Sports</span>
            </li>

            <li className="nav__item">
              <span className="nav__link nav__link--4">Stickers</span>
            </li>

            <li className="nav__item">
              <span className="nav__link nav__link--5">Artists</span>
            </li>

            {/* Dropdown Menu for Desktop */}
            <li className="nav__item">
              <span onMouseEnter={handleDropdownMenuDesktopHover} onMouseLeave={handleDropdownMenuDesktopHover} className="nav__link nav__link--6"><i className="fas fa-ellipsis-v"></i></span>
            </li>
          </ul>

          <div className="nav__buttons">
            <button className="nav__button">Upload</button>

            <button className="nav__button">Create</button>
          </div>

          <div className="nav__login">
            <div className="nav__login-icon-container">
              <i className="fas fa-user nav__login-icon"></i>
            </div>

            <div className="nav__button nav__button--login">Log in</div>
          </div>
				</div>
			</nav>

      { showMenuForDesktop && <DropdownMenu /> }
      { showMenuForMobile && <DropdownMenu /> }
    </header>
  )
}
