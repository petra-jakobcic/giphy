import React, { useState } from 'react';
import Masonry from 'react-masonry-css';

export default function SearchBar() {
  const [searchTerm, setSearchTerm] = useState("");
  const [searchResults, setSearchResults] = useState([]);

  /**
   * Handles the change in the search bar.
   *
   * @param {Object} e The event object.
   */
  function handleSearchTermChange(e) {
    setSearchTerm(e.target.value);
  }

  /**
   * Handles the 'Search' button click.
   *
   * @param {Object} e The event object.
   *
   * @returns {void}
   */
  function handleSearchButtonClick(e) {
    e.preventDefault();

    fetch(`https://api.giphy.com/v1/gifs/search?api_key=Dg8jRReIiU2Pn6FDIvX59hQpiLLd2Abq&q=${searchTerm}&limit=25&offset=0&rating=g&lang=en`)
    .then(response => response.json())
    // This contains the data, pagination and meta.
    .then(giphyResponse => giphyResponse.data)
    // This contains an array of all the gif objects.
    .then(gifArray => {
      return gifArray.map(gifObject => {
        return gifObject.images;
      });
    })
    // This contains an array of objects containing information about gifs.
    .then(gifObjects => {
      setSearchResults(gifObjects);
    });

    setSearchTerm("");
  }

  const breakpoints = {
    default: 3,
    1600: 3,
    1200: 2,
    610: 1
  }

  return (
    <section className="search-section u-margin-bottom-small">
      <div className="search-bar u-margin-bottom-small">
        <form onSubmit={handleSearchButtonClick} className="search-bar__input-container">
            <input type="text" className="search-bar__input" onChange={handleSearchTermChange} value={searchTerm} placeholder="Search all the GIFs and Stickers" />
        </form>

          <div className="search-bar__icon-container">
            <i className="fas fa-search search-bar__icon" onClick={handleSearchButtonClick}></i>
          </div>
      </div>

      <Masonry
        breakpointCols={breakpoints}
        className="gallery__grid"
        columnClassName="gallery__grid-column"
      >
        {
          searchResults.map((gifObject, i) => (
            <div key={i} className="gallery__gif-container u-margin-bottom-medium">
              <img src={gifObject.downsized_medium.url} alt="gif" height="auto" width="300" className="gallery__gif" />
            </div>
          ))
        }
      </Masonry>
    </section>
  )
}
