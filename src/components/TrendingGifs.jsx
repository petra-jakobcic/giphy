import React, { useState, useEffect } from 'react';
import Masonry from 'react-masonry-css';
import LoadingSpinner from './LoadingSpinner';

const limit = 25;

export default function TrendingGifs() {
  // State
  const [trendingGifs, setTrendingGifs] = useState([]);
  const [offset, setOffset] = useState(limit - 1);
  const [loading, setLoading] = useState(false);

  // Display the trending gifs on app load.
  useEffect(() => {
    fetch(`https://api.giphy.com/v1/gifs/trending?api_key=Dg8jRReIiU2Pn6FDIvX59hQpiLLd2Abq&limit=${limit}&rating=g`)
    .then(response => response.json())
    // This contains the data, pagination and meta.
    .then(giphyResponse => giphyResponse.data)
    // This contains an array of all the gif objects.
    .then(gifArray => {
      return gifArray.map(gifObject => {
        return gifObject.images;
      });
    })
    // This contains an array of objects containing information about gifs.
    .then(gifObjects => {
      setTrendingGifs(gifObjects);
    });
  }, []);

  /**
   * Handles the 'Load more' button click.
   *
   * @returns {void}
   */
  function handleLoadMoreButtonClick() {
    setLoading(true);

    setTimeout(() => {
      fetch(`https://api.giphy.com/v1/gifs/trending?api_key=Dg8jRReIiU2Pn6FDIvX59hQpiLLd2Abq&limit=${limit}&offset=${offset}&rating=g`)
      .then(response => response.json())
      // This contains the data, pagination and meta.
      .then(giphyResponse => giphyResponse.data)
      // This contains an array of all the gif objects.
      .then(gifArray => {
        return gifArray.map(gifObject => {
          return gifObject.images;
        });
      })
      // This contains an array of objects containing information about gifs.
      .then(gifObjects => {
        setTrendingGifs(trendingGifs.concat(gifObjects));
        setOffset(offset => offset + limit);
        setLoading(false);
      });
    }, 2000);
  }

  const breakpoints = {
    default: 3,
    1600: 3,
    1200: 2,
    610: 1
  }

  return (
    <div className="gallery">
      <div className="gallery__heading u-margin-bottom-small">
        <span>
          <i className="fas fa-chart-line gallery__heading-icon"></i>
        </span>

        <h3 className="gallery__heading-text">Trending</h3>
      </div>

      <Masonry
        breakpointCols={breakpoints}
        className="gallery__grid"
        columnClassName="gallery__grid-column"
      >
        {
          trendingGifs.map((gifObject, i) => (
            <div key={i} className="gallery__gif-container u-margin-bottom-medium">
              <img src={gifObject.downsized_medium.url} alt="gif" height="auto" width="300" className="gallery__gif" />
            </div>
          ))
        }
      </Masonry>

      {
        loading ?
        <LoadingSpinner /> :
        <div className="gallery__button-container u-margin-bottom-medium">
          <button className="gallery__button" onClick={handleLoadMoreButtonClick}>LOAD MORE GIFS</button>
        </div>
      }
    </div>
  );
}
