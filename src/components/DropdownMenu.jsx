import React from 'react';

export default function DropdownMenu() {
  return (
    <section className="dropdown">
      <div className="dropdown__sections">
        <div className="dropdown__section--categories">
          <h2 className="dropdown__heading u-margin-bottom-medium">Categories</h2>

          <div className="dropdown__links dropdown__links--animated">
            <ul>
              <li className="dropdown__link">GIPHY Studios</li>
              <li className="dropdown__link">Animals</li>
              <li className="dropdown__link">Actions</li>
              <li className="dropdown__link">Anime</li>
              <li className="dropdown__link">Cartoons</li>
              <li className="dropdown__link">Emotions</li>
            </ul>

            <ul>
              <li className="dropdown__link">Food/Drink</li>
              <li className="dropdown__link">Gaming</li>
              <li className="dropdown__link">Holidays/Greetings</li>
              <li className="dropdown__link">Memes</li>
              <li className="dropdown__link">Clips</li>
            </ul>
          </div>
        </div>

        <div className="dropdown__section--stickers">
          <h2 className="dropdown__heading u-margin-bottom-medium">Stickers</h2>

          <div className="dropdown__links dropdown__links--animated">
            <ul>
              <li className="dropdown__link">Originals</li>
              <li className="dropdown__link">Trending</li>
              <li className="dropdown__link">Reactions</li>
              <li className="dropdown__link">Packs</li>
            </ul>
          </div>
        </div>

        <div className="dropdown__section--apps">
          <h2 className="dropdown__heading u-margin-bottom-medium">Apps</h2>

          <div className="dropdown__links dropdown__links--animated">
            <ul>
              <li className="dropdown__link">GIPHY</li>
              <li className="dropdown__link">GIPHY World</li>
              <li className="dropdown__link">GIPHY Capture</li>
            </ul>
          </div>
        </div>

        <div className="dropdown__section--about">
          <h2 className="dropdown__heading u-margin-bottom-medium">About</h2>

          <div className="dropdown__links dropdown__links--animated">
            <ul>
                <li className="dropdown__link">Team</li>
                <li className="dropdown__link">Engineering Blog</li>
                <li className="dropdown__link">GIPHY Arts</li>
                <li className="dropdown__link">Studios</li>
                <li className="dropdown__link">Developers</li>
                <li className="dropdown__link">Labs</li>
              </ul>

              <ul>
                <li className="dropdown__link">FAQ</li>
                <li className="dropdown__link">Support</li>
                <li className="dropdown__link">Jobs</li>
                <li className="dropdown__link">DMCA</li>
                <li className="dropdown__link">Guidelines</li>
              </ul>
          </div>
        </div>
      </div>

      <div className="dropdown__copyright">
        <div className="dropdown__copyright-year">&copy; 2021 Petra</div>

        <ul className="dropdown__copyright-links">
          <li className="dropdown__copyright-link">Terms of Service</li>
          <li className="dropdown__copyright-link">Community Guidelines</li>
          <li className="dropdown__copyright-link">Privacy Policy</li>
          <li className="dropdown__copyright-link">Copyright</li>
          <li className="dropdown__copyright-link">Manage Cookies</li>
        </ul>
      </div>
    </section>
  )
}
