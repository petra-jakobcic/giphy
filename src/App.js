import './App.scss';
import Header from './components/Header';
import SearchBar from './components/SearchBar';
import Banner from './components/Banner';
import TrendingGifs from './components/TrendingGifs';

function App() {
  return (
    <div className="App">
      <Header />

      <SearchBar />

      <Banner />

      <TrendingGifs />
    </div>
  );
}

export default App;
