# Giphy by Petra ☀

This web app was created using a mobile-first approach.

## Installation

1. Clone the project:
   ```
   git clone https://bitbucket.org/petra-jakobcic/giphy.git
   ```
1. From the project folder, run `npm install`
1. Start the server with `npm start`
1. Enjoy! 😄

## Technologies used

- React
- Sass
- Masonry
- Browser fetch API